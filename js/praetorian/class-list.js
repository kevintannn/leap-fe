import { lecturer } from "../service/url.js";
import {config} from '../service/config.js'
import {logout} from '../service/auth.js'
import {isLecturer} from '../service/middleware.js'

const parseDateString = (s) => {
    var b = s.split(/\D/); 
    var d = new Date(b[0], --b[1], b[2]);
    const month = d.toLocaleString('default', { month: 'short' });
    return d.getDate() + " " + month + " " + d.getFullYear();
}

const generateCLNext = (course_name, date, begin_time, end_time, meeting_name) => {
    let b_time = begin_time.split(":")
    let e_time = end_time.split(":")
    let shift = b_time[0] + "." +b_time[1]+"-"+e_time[0] + "." +e_time[1]
    let d = parseDateString(date)
    return `<div class="d-flex align-content-center justify-content-center justify-content-around content-fixed"> 
                <h3 class = "widths1">NEXT CLASS</h3>   
                <h5 class = "widths2">${course_name}</h5> 
            </div>
            <div class="d-flex align-items-center justify-content-center  content-header ">
                <h5 class="width1">${d}</h5> 
                <h5 class="width2">${shift}</h5> 
                <h5 class="width3">${meeting_name}</h5></div> 
                </div>
        <i class="fas fa-angle-left fa-lg arrow not-press"></i>
         </div> `
}

const generateCLNextAll = (a, course_name, next_classes) => {
    let content = ""
    
    for(let i = 0; i < a; i++) {
        let b_time = next_classes[i].begin_time.split(":")
        let e_time = next_classes[i].end_time.split(":")
        let shift = b_time[0] + "." +b_time[1]+"-"+e_time[0] + "." +e_time[1]
        let meeting_name = next_classes[i].meeting_name
        let meeting_date = next_classes[i].meeting_date
        let d = parseDateString(meeting_date)
        content += `<div class=" d-flex flex-wrap flex-md-nowrap align-items-center justify-content-between ClassListInfoDetail" > 
                        <span><h5>${course_name}</h5></span> 
                        <h5>${d}</h5> 
                        <h5>${shift}</h5> 
                        <h5>${meeting_name}</h5> 
                    </div>`
    }
    return content
}

const generateNextUI = (a, course_name, next_classes) => {
    let cLH = `<div class=" col-10 col-md-9 classListHeader" id="btnDown${a}">`
    let cLI = `<div class=" d-flex align-items-center ClassListInfo" ><div class="d-flex align-items-center justify-content-around classListStyle" >`

    let cLNext = generateCLNext(course_name, next_classes[0].meeting_date, next_classes[0].begin_time, next_classes[0].end_time, next_classes[0].meeting_name)
    
    let cLNextAll = generateCLNextAll(next_classes.length, course_name, next_classes)

    let cLE = `</div>`

    let cLAttendance = `<div class="col-10 col-md-2  offset-md-1 rightContent btn-attend">
    <a href="attendance.html" class="btn icon">Attendance</a>
    </div>`

    $("#containerRow").append(cLH + cLI + cLNext + cLNextAll + cLAttendance + cLE + cLAttendance);
    
    buttonHandler($("#btnDown"+a));

    let x = window.matchMedia("(max-width: 767px)")

    let height = 74 * (next_classes.length + 1);

    function buttonHandler(btn) {
        btn.click(function () {

            if ($(this).css("height") === "74px" && x.matches) {
                $(this).css("height", "700px");
                $(this)
                    .find("i")
                    .addClass("press");
            }
            else if ($(this).css("height") === "74px") {
                $(this).css({
                    "height": height + "px",
                    "transition": "0.5s"
                });

                $(this)
                    .find("i")
                    .addClass("press");

            } else {
                $(this).css({
                    "height": "74px",
                    "transition": "0.5s"
                });
                $(this)
                    .find("i")
                    .removeClass("press");
            }
        });
    }
}

const generateCLUpcoming = (course_name, date, begin_time, end_time, meeting_name) => {
    let b_time = begin_time.split(":")
    let e_time = end_time.split(":")
    let shift = b_time[0] + "." +b_time[1]+"-"+e_time[0] + "." +e_time[1]
    let d = parseDateString(date)
    return `<div class="d-flex align-content-center justify-content-center justify-content-around content-fixed"> 
                <h3 class = "widths1"><b>UPCOMING</b></h3>  
                <h5 class = "widths2">${course_name}</h5> 
            </div>
            <div class="d-flex align-items-center justify-content-center  content-header ">
                <h5 class = "width1">${d}</h5> 
                <h5 class = "width2">${shift}</h5> 
                <h5 class = "width3">${meeting_name}</h5></div> 
                </div>
           <i class="fas fa-angle-left fa-lg arrows not-press"></i>
            </div> `
}

const generateCLUpcomingAll = (a, course_name, next_classes) => {
    let content = ""
    
    for(let i = 0; i < a; i++) {
        let b_time = next_classes[i].begin_time.split(":")
        let e_time = next_classes[i].end_time.split(":")
        let shift = b_time[0] + "." +b_time[1]+"-"+e_time[0] + "." +e_time[1]
        let meeting_name = next_classes[i].meeting_name
        let meeting_date = next_classes[i].meeting_date
        let d = parseDateString(meeting_date)
        content += `<div class=" d-flex flex-wrap flex-md-nowrap align-items-center justify-content-between ClassListInfoDetail font-color" > 
                        <span><h5>${course_name}</h5></span> 
                        <h5>${d}</h5> 
                        <h5>${shift}</h5> 
                        <h5>${meeting_name}</h5> 
                    </div>`
    }
    return content
}

const generateUpcomingUI = (a, course_name, next_classes) => {
    let cLH = `<div class=" col-10 col-md-9 classListHeaders" id="btnDown${a}">`
    let cLI = `<div class=" d-flex align-items-center ClassListInfos" > <div class="d-flex align-items-center justify-content-around classListStyles" >`

    let cLNext = generateCLUpcoming(course_name, next_classes[0].meeting_date, next_classes[0].begin_time, next_classes[0].end_time, next_classes[0].meeting_name)
    
    let cLNextAll = generateCLUpcomingAll(next_classes.length, course_name, next_classes)

    let cLE = `</div>`

    let cLAttendance = `<div class="col-10 col-md-2  offset-md-1 rightContent rightContentColor" >
    <button type="button" class="btn icon" data-toggle="modal" data-target="#modal"> Reschedule  </button></div>`

    let modal = `<!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Reschedule Class</h3>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <labe class="col-form-label">
                                <h4>Select Class</h4>
                                </label>
                                <select class="form-control form-container" id="exampleFormControlSelect1">

                                <option>Trial Clas 1</option>
                                <option>Trial Clas 2</option>
                                <option>Trial Clas 3</option>
                                <option>Trial Clas 4</option>
                                <option>Trial Clas 5</option>
                            </select>
                        </div>

                        <div class="form-groups">
                            <div class="form-group">
                                <label class="col-form-label"><h4>I want this class to be reschedule to</h4></label>
                                <select class="form-control form-container" id="exampleFormControlSelect1">

                                <option>Trial Clas 1</option>
                                <option>Trial Clas 2</option>
                                <option>Trial Clas 3</option>
                                <option>Trial Clas 4</option>
                                <option>Trial Clas 5</option>
                            </select>
                            </div>

                            <div class="form-group">
                                <label class="col-form-label"><h4  id="shiftFont">Shift</h4></label>
                                <select class="form-control form-container" id="exampleFormControlSelect1">

                                <option>Trial Clas 1</option>
                                <option>Trial Clas 2</option>
                                <option>Trial Clas 3</option>
                                <option>Trial Clas 4</option>
                                <option>Trial Clas 5</option>
                            </select>
                            </div>
                        </div>



                        <div class="footer">
                            <input type="submit" class="btnReschedule" value="Reschedule"></input>

                            <input type="submit" class="btnCancel" value="Cancel"></input>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>`

    $("#containerRow").append(cLH + cLI + cLNext + cLNextAll + cLAttendance + cLE + cLAttendance + modal);
    
    buttonHandler($("#btnDown"+a));

    let x = window.matchMedia("(max-width: 767px)")

    let height = 74 * (next_classes.length + 1);

    function buttonHandler(btn) {
        btn.click(function () {

            if ($(this).css("height") === "74px" && x.matches) {
                $(this).css("height", "700px");
                $(this)
                    .find("i")
                    .addClass("press");
            }
            else if ($(this).css("height") === "74px") {
                $(this).css({
                    "height": height + "px",
                    "transition": "0.5s"
                });

                $(this)
                    .find("i")
                    .addClass("press");

            } else {
                $(this).css({
                    "height": "74px",
                    "transition": "0.5s"
                });
                $(this)
                    .find("i")
                    .removeClass("press");
            }
        });
    }
}

const getLecturerClassList = () => {
	axios.get(lecturer["class-list"], 
		config.auth
	)
		.then((response) => {
            
        for(let i = 0; i < response.data.lecturer_classes.length; i++) {
            if (i==0) {
                generateNextUI(i, response.data.lecturer_classes[i].course_name, 
                    response.data.lecturer_classes[i].next_classes)
            } else {
                generateUpcomingUI(i, response.data.lecturer_classes[i].course_name, 
                    response.data.lecturer_classes[i].next_classes)
            }
            
        }

        let class_schedule_id = response.data.lecturer_classes[0].next_classes[0].id;
        checkAttendanceStatus(class_schedule_id);
        
        console.log(response.data.lecturer_classes[0].next_classes[0].id);
	}).catch(function (error) {
		console.log(error);
	});
}

const checkAttendanceStatus = (class_schedule_id) => {
    axios.post(lecturer["check-attendance"], {
        'class_schedule_id': class_schedule_id
	}, config.auth)
	.then(function (response) {
        if(response.data.submitted) {
            $(".btn-attend").remove("");
            // $(".btn-attend").html("");
            // let c = `<a>--</a>`
            // $(".btn-attend").html(c);
        }
	})
	.catch(function (error) {
        console.log(error)
	});
}

$(document).ready(function () {
    if(!isLecturer()){
        alert("You Are Not Allowed to Access This Page")
        logout()
    } else {
        getLecturerClassList()
        $('#btn-logout').click(function() {
            logout()
        });
    }
});